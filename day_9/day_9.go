package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strconv"
	"strings"

	mapset "github.com/deckarep/golang-set/v2"
)

type Instruction struct {
	direction string
	steps     uint64
}

type Coordinate struct {
	x int64
	y int64
}

func Add(position Coordinate, offset Coordinate) Coordinate {
	return Coordinate{
		x: position.x + offset.x,
		y: position.y + offset.y,
	}
}

func Distance(position1 Coordinate, position2 Coordinate) float64 {

	x1 := float64(position1.x)
	y1 := float64(position1.y)
	x2 := float64(position2.x)
	y2 := float64(position2.y)

	// Calculate the Manhattan distance between two coordinates
	distance := math.Sqrt(math.Pow(x2-x1, 2) + math.Pow(y2-y1, 2))
	// fmt.Printf("%f, %f, %f, %f => %f\n", x1, y1, x2, y2, distance)
	return distance

}

func loadInstructions(path string) []Instruction {
	f, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	var instructions []Instruction = make([]Instruction, 0)

	for scanner.Scan() {
		text := scanner.Text()
		parts := strings.Split(text, " ")
		steps, _ := strconv.ParseUint(parts[1], 0, 64)
		instructions = append(instructions, Instruction{direction: parts[0], steps: steps})

	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return instructions
}

func main() {

	// Read the file into an Array of strings.
	instructions := loadInstructions(os.Args[1])
	ropeLength := 10

	tailPositions := mapset.NewSet[Coordinate]()

	var rope [10]Coordinate = [10]Coordinate{
		{
			x: 0, y: 0,
		},
		{
			x: 0, y: 0,
		},
		{
			x: 0, y: 0,
		},
		{
			x: 0, y: 0,
		},
		{
			x: 0, y: 0,
		},
		{
			x: 0, y: 0,
		},
		{
			x: 0, y: 0,
		},
		{
			x: 0, y: 0,
		},
		{
			x: 0, y: 0,
		},
		{
			x: 0, y: 0,
		},
	}

	tailPositions.Add(rope[ropeLength-1])

	fmt.Printf("Head: %d, Tail: %d\n", rope[0], rope[ropeLength-1])

	for instructionNumber, instruction := range instructions {

		fmt.Printf("%d - ", instructionNumber)

		for index := 0; uint64(index) < instruction.steps; index++ {

			var headOffset Coordinate
			if instruction.direction == "U" {
				headOffset = Coordinate{x: 0, y: 1}
			} else if instruction.direction == "D" {
				headOffset = Coordinate{x: 0, y: -1}
			} else if instruction.direction == "L" {
				headOffset = Coordinate{x: -1, y: 0}
			} else if instruction.direction == "R" {
				headOffset = Coordinate{x: 1, y: 0}
			}

			// Calculate new position for head
			rope[0] = Add(rope[0], headOffset)

			// Calculate new position for tail
			for ropeIndex := 1; ropeIndex < ropeLength; ropeIndex++ {
				if Distance(rope[ropeIndex-1], rope[ropeIndex]) >= 2 {

					if rope[ropeIndex-1].y > rope[ropeIndex].y {
						rope[ropeIndex].y += 1
					} else if rope[ropeIndex-1].y < rope[ropeIndex].y {
						rope[ropeIndex].y -= 1
					}

					if rope[ropeIndex-1].x > rope[ropeIndex].x {
						rope[ropeIndex].x += 1
					} else if rope[ropeIndex-1].x < rope[ropeIndex].x {
						rope[ropeIndex].x -= 1
					}

				}
			}

			tailPositions.Add(rope[ropeLength-1])

			fmt.Printf("Head: %d, ", rope[0])
			for ropeIndex := 1; ropeIndex < ropeLength; ropeIndex++ {
				fmt.Printf("%d: %d, ", ropeIndex, rope[ropeIndex])
			}
			fmt.Println()
		}

	}
	fmt.Printf("The tail has visited %d positions.\n", tailPositions.Cardinality())
}
