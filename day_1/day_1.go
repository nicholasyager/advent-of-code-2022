package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
)

func main() {
	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	var elves []uint64 = make([]uint64, 0)
	var accumulatedCalories uint64 = 0

	for scanner.Scan() {
		text := scanner.Text()

		if text != "" {
			calories, _ := strconv.ParseUint(text, 0, 19)
			accumulatedCalories += calories
		} else {
			elves = append(elves, accumulatedCalories)
			accumulatedCalories = 0
		}

	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	// Sort the elves array in decending order
	sort.Slice(elves, func(i, j int) bool {
		return elves[i] > elves[j]
	})

	// Print our answers!
	fmt.Printf("The elf with the most Calories has %d calories.\n", elves[0])
	fmt.Printf("The 3 elves with the most Calories have a combined %d calories.\n", elves[0]+elves[1]+elves[2])

}
