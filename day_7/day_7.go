package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

type Directory struct {
	name           string
	path           string
	size           uint64
	contentSize    uint64
	contents       []string
	subdirectories []string
}

func AddFileToDir(parentFile Directory, key string, size uint64) Directory {
	parentFile.contents = append(parentFile.contents, key)
	parentFile.contentSize += size
	return parentFile
}

func AddDirToDir(parentDirectory Directory, key string) Directory {
	parentDirectory.subdirectories = append(parentDirectory.subdirectories, key)
	return parentDirectory
}

func CalculateDirectorySize(filesystem map[string]Directory, directory Directory) uint64 {

	directory.size = directory.contentSize

	for _, subdirectoryPath := range directory.subdirectories {
		subdirectory := filesystem[subdirectoryPath]
		subdirectorySize := CalculateDirectorySize(filesystem, subdirectory)
		directory.size += subdirectorySize
	}

	filesystem[directory.path] = directory

	return directory.size
}

func main() {

	var filesystem map[string]Directory = map[string]Directory{}

	// Read the file into an Array of strings.
	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	var directoryStack []string = make([]string, 0)

	cdRegex := regexp.MustCompile("\\$ cd ([a-zA-Z0-9/\\.]+)")
	dirRegex := regexp.MustCompile("dir ([a-zA-Z0-9/\\.]+)")
	fileRegex := regexp.MustCompile("([0-9]+) ([a-zA-Z0-9/\\.]+)")

	for scanner.Scan() {
		text := scanner.Text()
		fmt.Println(text)

		var currentDirectory = strings.Join(directoryStack, "/")

		if strings.HasPrefix(text, "$ cd") {
			parts := cdRegex.FindStringSubmatch(text)
			directory := parts[1]

			// move up the stack 1
			if directory == ".." {
				directoryStack = directoryStack[:len(directoryStack)-1]
			} else {
				directoryStack = append(directoryStack, directory)
			}
			fmt.Println(directoryStack)
		} else if strings.HasPrefix(text, "dir ") {
			// This is a dir! Add it to the contents of the current File in the stack.
			parts := dirRegex.FindStringSubmatch(text)
			newDirectory := Directory{
				name:           parts[1],
				path:           currentDirectory + "/" + parts[1],
				size:           0,
				contentSize:    0,
				contents:       make([]string, 0),
				subdirectories: make([]string, 0),
			}
			filesystem[currentDirectory] = AddDirToDir(filesystem[currentDirectory], newDirectory.path)
			filesystem[newDirectory.path] = newDirectory
		} else if strings.HasPrefix(text, "$ ls") {
			// Listing. Skip it all. lol
		} else {
			// This is a file! Add it to the current directory, and increment the content size!
			parts := fileRegex.FindStringSubmatch(text)
			size, _ := strconv.ParseUint(parts[1], 0, 64)

			filesystem[currentDirectory] = AddFileToDir(filesystem[currentDirectory], parts[2], size)
		}

	}
	// Traverse the virtual file system, and calculate all of the directory sizes.
	usedSpace := CalculateDirectorySize(filesystem, filesystem["/"])
	fmt.Printf("Total filesystem size: %d\n", usedSpace)

	var output uint64 = 0
	var filesystemSize uint64 = 70000000
	var unusedSpace = filesystemSize - usedSpace
	var target uint64 = 30000000

	var deletionDirectorySize uint64 = filesystemSize

	for _, directory := range filesystem {
		if directory.size <= 100000 {
			output += directory.size
		}

		if unusedSpace+directory.size >= target {
			if directory.size < deletionDirectorySize {
				deletionDirectorySize = directory.size
				fmt.Printf("New smallest deletable file found! %s (%d)\n", directory.path, directory.size)
			}
		}

	}

}
