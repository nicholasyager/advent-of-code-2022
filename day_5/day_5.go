package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func loadStacks(path string) [][]string {
	f, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	var stack [][]string = make([][]string, 0)

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		text := scanner.Text()

		stack = append(stack, strings.Split(text, ""))

	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return stack
}

func main() {

	stacks := loadStacks(os.Args[1])
	var stacks_part_two [][]string = make([][]string, 0)
	for _, stack := range stacks {
		stacks_part_two = append(stacks_part_two, stack)

	}

	// Read the file into an Array of strings.
	// var lines []string = make([]string, 0)
	f, err := os.Open(os.Args[2])
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	re := regexp.MustCompile("move (\\d+) from (\\d+) to (\\d+)")

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		text := scanner.Text()
		parts := re.FindStringSubmatch(text)

		count, _ := strconv.ParseInt(parts[1], 0, 64)
		raw_source, _ := strconv.ParseUint(parts[2], 0, 64)
		raw_destination, _ := strconv.ParseUint(parts[3], 0, 64)

		source := raw_source - 1
		destination := raw_destination - 1

		for i := uint64(0); i < uint64(count); i++ {

			item := stacks[source][len(stacks[source])-1]

			stacks[source] = stacks[source][:len(stacks[source])-1]
			stacks[destination] = append(stacks[destination], item)
		}

		position := int64(len(stacks_part_two[source]))

		var items = stacks_part_two[source][position-count : position]

		for _, item := range items {
			stacks_part_two[destination] = append(stacks_part_two[destination], item)
		}

		stacks_part_two[source] = stacks_part_two[source][:position-count]

	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	for _, stack := range stacks {
		fmt.Printf("%s", stack[len(stack)-1])
	}
	fmt.Println("")

	for _, stack := range stacks_part_two {
		fmt.Printf("%s", stack[len(stack)-1])
	}

}
