package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type CycleData struct {
	start int64
	end   int64
}

type CPUInstruction struct {
	operation string
	value     int64
}

func loadCPUInstructions(path string) []CPUInstruction {
	f, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	var instructions []CPUInstruction = make([]CPUInstruction, 0)

	for scanner.Scan() {
		text := scanner.Text()
		parts := strings.Split(text, " ")
		var steps int64 = 0
		if len(parts) > 1 {
			steps, _ = strconv.ParseInt(parts[1], 0, 64)
		}
		instructions = append(instructions, CPUInstruction{operation: parts[0], value: steps})

	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return instructions
}

func main() {

	// Read the file into an Array of strings.
	instructions := loadCPUInstructions(os.Args[1])
	var cycles []CycleData = make([]CycleData, 0)

	var register int64 = 1

	for instructionNumber, instruction := range instructions {

		fmt.Printf("%d - %s\n", instructionNumber, instruction)

		if instruction.operation == "noop" {
			cycles = append(cycles, CycleData{start: register, end: register})
		} else if instruction.operation == "addx" {
			cycles = append(cycles, CycleData{start: register, end: register})
			cycles = append(cycles, CycleData{start: register, end: register + instruction.value})
			register += instruction.value

		}

	}

	for index, values := range cycles {
		fmt.Printf("%d - %d\n", index, values)
	}

	var output int64 = 0

	for _, index := range [6]int64{19, 59, 99, 139, 179, 219} {
		value := (index + 1) * cycles[index].start
		output += value
		fmt.Printf("%d -> %d * %d = %d => %d\n", index, index+1, cycles[index].start, value, output)
	}

	// 20th, 60th, 100th, 140th, 180th, and 220th cycles
	fmt.Printf("Output data: %d\n", output)

	for cycleIndex, data := range cycles {

		var position = int64(cycleIndex) % 40

		if position == 0 {
			fmt.Println()
		}

		if position >= data.start-1 && position <= data.start+1 {
			fmt.Printf("#")
		} else {
			fmt.Printf(".")
		}

	}

}
