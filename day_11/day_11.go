package main

import (
	"fmt"
	"sort"
)

type MonkeyOperation func(uint64) uint64
type MonkeyTest func(uint64) uint64

type Monkey struct {
	items          []uint64
	operation      MonkeyOperation
	test           uint64
	targets        []uint64
	itemsInspected uint64
}

func main() {

	steps := 10000

	// Test case monkeys
	// monkeys := [4]Monkey{

	// 	{
	// 		items:     []uint64{79, 98},
	// 		operation: func(i uint64) uint64 { return i * 19 },
	// 		test:      23,
	// 		targets:   []uint64{2, 3},
	// 	},
	// 	{
	// 		items:     []uint64{54, 65, 75, 74},
	// 		operation: func(i uint64) uint64 { return i + 6 },
	// 		test:      19,
	// 		targets:   []uint64{2, 0},
	// 	},
	// 	{
	// 		items:     []uint64{79, 60, 97},
	// 		operation: func(i uint64) uint64 { return i * i },
	// 		test:      13,
	// 		targets:   []uint64{1, 3},
	// 	},
	// 	{
	// 		items:     []uint64{74},
	// 		operation: func(i uint64) uint64 { return i + 3 },
	// 		test:      17,
	// 		targets:   []uint64{0, 1},
	// 	},
	// }

	monkeys := [8]Monkey{

		{
			items:     []uint64{66, 71, 94},
			operation: func(i uint64) uint64 { return i * 5 },
			test:      3,
			targets:   []uint64{7, 4},
		},
		{
			items:     []uint64{70},
			operation: func(i uint64) uint64 { return i + 6 },
			test:      17,
			targets:   []uint64{3, 0},
		},
		{
			items:     []uint64{62, 68, 56, 65, 94, 78},
			operation: func(i uint64) uint64 { return i + 5 },
			test:      2,
			targets:   []uint64{3, 1},
		},
		{
			items:     []uint64{89, 94, 94, 67},
			operation: func(i uint64) uint64 { return i + 2 },
			test:      19,
			targets:   []uint64{7, 0},
		},
		{
			items:     []uint64{71, 61, 73, 65, 98, 98, 63},
			operation: func(i uint64) uint64 { return i * 7 },
			test:      11,
			targets:   []uint64{5, 6},
		},
		{
			items:     []uint64{55, 62, 68, 61, 60},
			operation: func(i uint64) uint64 { return i + 7 },
			test:      5,
			targets:   []uint64{2, 1},
		},
		{
			items:     []uint64{93, 91, 69, 64, 72, 89, 50, 71},
			operation: func(i uint64) uint64 { return i + 1 },
			test:      13,
			targets:   []uint64{5, 2},
		},
		{
			items:     []uint64{76, 50},
			operation: func(i uint64) uint64 { return i * i },
			test:      7,
			targets:   []uint64{4, 6},
		},
	}

	for step := 0; step < steps; step++ {

		fmt.Printf("Step %d\n", step)

		var totalItems = 0
		for _, monkey := range monkeys {
			totalItems += len(monkey.items)
		}

		var commonMultiple uint64 = 1
		for _, monkey := range monkeys {
			commonMultiple *= monkey.test
		}

		for monekyIndex, monkey := range monkeys {

			for true {

				if len(monkeys[monekyIndex].items) == 0 {
					break
				}

				monkeys[monekyIndex].itemsInspected++

				item := monkeys[monekyIndex].items[0]
				monkeys[monekyIndex].items = monkeys[monekyIndex].items[1:]

				item = monkey.operation(item)
				if item > commonMultiple {
					item = item % commonMultiple
				}

				var index uint64
				if item%monkey.test == 0 {
					index = monkey.targets[0]
				} else {
					index = monkey.targets[1]
				}

				monkeys[index].items = append(monkeys[index].items, item)

			}

		}

	}

	for index, monkey := range monkeys {

		fmt.Printf("%d -> %d\n", index, monkey.itemsInspected)

	}

	sort.Slice(monkeys[:], func(i, j int) bool {
		return monkeys[i].itemsInspected > monkeys[j].itemsInspected
	})
	fmt.Printf("Monkeybusiness: %d\n", monkeys[0].itemsInspected*monkeys[1].itemsInspected)

}
