package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"

	mapset "github.com/deckarep/golang-set/v2"
)

func MakeSet(assignment string) mapset.Set[uint64] {
	rangeStrings := strings.Split(assignment, "-")
	start, _ := strconv.ParseUint(rangeStrings[0], 0, 64)
	end, _ := strconv.ParseUint(rangeStrings[1], 0, 64)
	set := mapset.NewSet[uint64]()
	for index := start; index <= end; index++ {
		set.Add(index)
	}
	return set
}

func main() {

	// Read the file into an Array of strings.
	// var lines []string = make([]string, 0)
	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	var completeOverlaps uint64 = 0
	var overlaps uint64 = 0

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		text := scanner.Text()

		parts := strings.Split(text, ",")
		elf1 := MakeSet(parts[0])
		elf2 := MakeSet(parts[1])

		if elf1.IsSubset(elf2) || elf2.IsSubset(elf1) {
			completeOverlaps += 1
			overlaps += 1
		} else if elf1.Intersect(elf2).Cardinality() > 0 {
			overlaps += 1
		}

	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	fmt.Printf("Complete Overlaps: %d\n", completeOverlaps)
	fmt.Printf("Total Overlaps: %d\n", overlaps)
}
