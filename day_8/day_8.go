package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

func loadForest(path string) [][]uint64 {
	f, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	var stack [][]uint64 = make([][]uint64, 0)

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		text := scanner.Text()

		var row []uint64 = make([]uint64, 0)
		for _, char := range strings.Split(text, "") {
			integer, _ := strconv.ParseUint(char, 0, 64)
			row = append(row, integer)
		}
		stack = append(stack, row)

	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return stack
}

func Reverse(input []uint64) []uint64 {
	inputLen := len(input)
	output := make([]uint64, inputLen)

	for i, n := range input {
		j := inputLen - i - 1

		output[j] = n
	}

	return output
}

func main() {

	// Read the file into an Array of strings.
	forest := loadForest(os.Args[1])
	var inverseForest [][]uint64

	for col := 0; col <= len(forest[0])-1; col++ {
		var column []uint64 = make([]uint64, 0)
		for row := 0; row <= len(forest)-1; row++ {
			column = append(column, forest[row][col])
		}
		inverseForest = append(inverseForest, column)
	}

	maxRow := len(forest) - 1
	maxCol := len(forest[0]) - 1

	for row := 0; row <= maxRow; row++ {
		for col := 0; col <= maxCol; col++ {
			fmt.Printf("%d", forest[row][col])
		}
		fmt.Println()
	}

	fmt.Println()

	for row := 0; row <= maxRow; row++ {
		for col := 0; col <= maxCol; col++ {
			fmt.Printf("%d", inverseForest[row][col])
		}
		fmt.Println()
	}

	visibleTrees := 0
	var scores []uint64 = make([]uint64, 0)

	for row := 0; row <= maxRow; row++ {
		for col := 0; col <= maxCol; col++ {

			var visibleDirections int = 4

			value := forest[row][col]

			minSearchRow := row + 1
			if minSearchRow > len(forest[0]) {
				minSearchRow = len(forest[0])
			}

			maxSearchRow := row
			if maxSearchRow < 0 {
				maxSearchRow = 0
			}

			minSearchCol := col + 1
			if minSearchCol >= len(forest) {
				minSearchCol = len(forest)
			}

			maxSearchCol := col
			if maxSearchCol < 0 {
				maxSearchCol = 0
			}

			left := Reverse(forest[row][:maxSearchCol])
			right := forest[row][minSearchCol:]
			top := Reverse(inverseForest[col][:maxSearchRow])
			bottom := inverseForest[col][minSearchRow:]

			fmt.Println(value)
			fmt.Printf("0->%d %d->end 0->%d %d->max\n", minSearchRow, maxSearchRow, minSearchCol, maxSearchCol)

			var neighbors = [4][]uint64{left, right, top, bottom}
			var scenicScore = 1
			for _, neighborList := range neighbors {
				fmt.Printf("%d => ", neighborList)
				var isVisible = true

				if len(neighborList) == 0 {
					scenicScore *= 0
				}

				var score = 0
				for index, neighbor := range neighborList {
					score = index + 1
					if neighbor >= value {
						visibleDirections -= 1
						isVisible = false
						break
					}

				}
				scenicScore *= score
				fmt.Printf(" %d", score)

				if isVisible {
					fmt.Printf(" [VISIBLE]")
				}
				fmt.Println()

			}

			fmt.Printf(" Score: %d", scenicScore)
			scores = append(scores, uint64(scenicScore))
			fmt.Println()

			// fmt.Printf("%d", value)

			if visibleDirections > 0 {
				visibleTrees += 1
			}

			fmt.Println()

		}
		fmt.Println()
	}

	fmt.Printf("Visible Trees: %d\n", visibleTrees)

	sort.Slice(scores, func(p, q int) bool {
		return scores[p] > scores[q]
	})

	fmt.Printf("Largest Scenic Score: %d\n", scores[0])
}
