package main

import (
	"fmt"
	"io"
	"log"
	"os"

	mapset "github.com/deckarep/golang-set/v2"
)

func MakeSet(signal []byte) mapset.Set[byte] {
	set := mapset.NewSet[byte]()
	for _, byte := range signal {
		set.Add(byte)
	}
	return set
}

func main() {

	// Read the file into an Array of strings.
	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	// declare chunk size
	const maxSz = 1

	// create buffer
	b := make([]byte, maxSz)

	var signal []byte = make([]byte, 0)
	var index = 0

	startOfPacketFound := false

	for {
		// read content to buffer
		_, err := f.Read(b)
		if err != nil {
			if err != io.EOF {
				fmt.Println(err)
			}
			break
		}

		signal = append(signal, b[0])

		var packetSize = 4
		if startOfPacketFound == false {
			// Lol. This should be a function. Nothing to see here.
			startPosition := len(signal) - packetSize
			if startPosition < 0 {
				startPosition = 0
			}

			latestQuartet := signal[startPosition:]
			startOfPacket := MakeSet(latestQuartet)

			if startOfPacket.Cardinality() == packetSize {
				fmt.Printf("Found start-of-packet marker (%s) at position %d.\n", string(latestQuartet), startPosition+packetSize)
				startOfPacketFound = true
			}
		} else {
			var messageSize = 14
			startMessagePosition := len(signal) - messageSize
			if startMessagePosition < 0 {
				startMessagePosition = 0
			}

			latestChunk := signal[startMessagePosition:]
			startOfPacket := MakeSet(latestChunk)

			if startOfPacket.Cardinality() == messageSize {
				fmt.Printf("Found start-of-message marker (%s) at position %d.\n", string(latestChunk), startMessagePosition+messageSize)
				break
			}
		}

		index++
	}

}
