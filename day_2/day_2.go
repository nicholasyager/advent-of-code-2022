package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

const (
	Rock     uint64 = 1
	Paper           = 2
	Scissors        = 3
)

const (
	Win  uint64 = 6
	Draw        = 3
	Lose        = 0
)

func main() {

	characterMoves := map[string]uint64{
		"A": Rock,
		"B": Paper,
		"C": Scissors,
		"X": Rock,
		"Y": Paper,
		"Z": Scissors,
	}

	outcomes := map[string]uint64{
		"X": Lose,
		"Y": Draw,
		"Z": Win,
	}

	var score uint64 = 0

	// Read the file into an Array of strings.
	var lines []string = make([]string, 0)
	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		text := scanner.Text()
		lines = append(lines, text)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	// Part One
	for _, text := range lines {
		fields := strings.Fields(text)

		var outcome uint64 = Lose

		myMove := characterMoves[fields[1]]
		opponentMove := characterMoves[fields[0]]

		if opponentMove == Rock {
			if myMove == Paper {
				outcome = Win
			} else if myMove == Rock {
				outcome = Draw
			}
		} else if opponentMove == Paper {
			if myMove == Scissors {
				outcome = Win
			} else if myMove == Paper {
				outcome = Draw
			}
		} else if opponentMove == Scissors {
			if myMove == Rock {
				outcome = Win
			} else if myMove == Scissors {
				outcome = Draw
			}
		}

		score += myMove + outcome

	}

	fmt.Printf("# Part One: Assume column two is my Move.\n")
	fmt.Printf("Score: %d\n", score)

	// Part Two

	score = 0
	fmt.Printf("# Part Two: Column two is the outcome of the match.\n")
	for _, text := range lines {
		fields := strings.Fields(text)

		opponentMove := characterMoves[fields[0]]
		outcome := outcomes[fields[1]]
		var myMove uint64 = opponentMove

		if opponentMove == Rock {
			if outcome == Win {
				myMove = Paper
			} else if outcome == Lose {
				myMove = Scissors
			}
		} else if opponentMove == Paper {
			if outcome == Win {
				myMove = Scissors
			} else if outcome == Lose {
				myMove = Rock
			}
		} else if opponentMove == Scissors {
			if outcome == Win {
				myMove = Rock
			} else if outcome == Lose {
				myMove = Paper
			}
		}

		score += myMove + outcome

	}

	fmt.Printf("Score: %d\n", score)

}
