package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"strings"

	mapset "github.com/deckarep/golang-set/v2"
)

func loadMap(path string) Terrain {

	letters := "SabcdefghijklmnopqrstuvwxyzE"

	f, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)

	var terrain [][]uint64 = make([][]uint64, 0)

	for scanner.Scan() {
		text := scanner.Text()
		characters := strings.Split(text, "")
		var row []uint64 = make([]uint64, 0)
		for _, character := range characters {
			row = append(row, uint64(strings.Index(letters, character)))
		}
		terrain = append(terrain, row)

	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return terrain
}

func PrintTerrain(terrain [][]uint64) {
	for row := 0; row < len(terrain); row++ {
		for col := 0; col < len(terrain[0]); col++ {
			fmt.Printf("%2d ", terrain[row][col])
		}
		fmt.Println()
	}
}

type Terrain [][]uint64

type Coordinate struct {
	x int64
	y int64
}

func (coordinate *Coordinate) Distance(coordinate2 Coordinate) float64 {
	return math.Sqrt(math.Pow(float64(coordinate2.x-coordinate.x), 2) + math.Pow(float64(coordinate2.y-coordinate.y), 2))
}

func (coordinate *Coordinate) Add(coordinate2 Coordinate) Coordinate {
	return Coordinate{
		x: coordinate.x + coordinate2.x,
		y: coordinate.y + coordinate2.y,
	}
}

func (terrain Terrain) GetHeight(coordinate Coordinate) uint64 {
	return terrain[coordinate.y][coordinate.x]
}

func reconstructPath(cameFrom map[Coordinate]Coordinate, current Coordinate) []Coordinate {
	var totalPath []Coordinate = []Coordinate{current}
	for true {

		value, isMapContainsKey := cameFrom[current]

		if isMapContainsKey {
			current = value
			totalPath = append([]Coordinate{current}, totalPath...)
		} else {
			break
		}

	}

	return totalPath

}

func PrintPath(terrain [][]uint64, gScore map[Coordinate]uint64, path []Coordinate) {

	// letters := "SabcdefghijklmnopqrstuvwxyzE"
	var pathSet = mapset.NewSet[Coordinate]()
	for _, coord := range path {
		pathSet.Add(coord)
	}

	for row := 0; row < len(terrain); row++ {
		for col := 0; col < len(terrain[0]); col++ {

			coord := Coordinate{
				x: int64(col),
				y: int64(row),
			}

			_, viewed := gScore[coord]

			if viewed {
				if pathSet.Contains(coord) {
					fmt.Printf("#")
				} else {
					fmt.Printf(".")
				}

			} else {
				fmt.Printf(".")
			}

		}
		fmt.Println()
	}

	fmt.Println()
}

func PrintSearch(terrain [][]uint64, gScore map[Coordinate]uint64) {

	letters := "SabcdefghijklmnopqrstuvwxyzE"

	for row := 0; row < len(terrain); row++ {
		for col := 0; col < len(terrain[0]); col++ {

			coord := Coordinate{
				x: int64(col),
				y: int64(row),
			}

			_, viewed := gScore[coord]

			if viewed {

				fmt.Printf("%s", string(letters[terrain[row][col]]))

			} else {
				fmt.Printf(".")
			}

		}
		fmt.Println()
	}

	fmt.Println()
}

func main() {

	// Read the file into an Array of strings.
	terrain := loadMap(os.Args[1])

	// Find the start
	var startCoord Coordinate
	var endCoord Coordinate

	for row := 0; row < len(terrain); row++ {
		for col := 0; col < len(terrain[0]); col++ {
			if terrain[row][col] == 0 {
				endCoord = Coordinate{
					y: int64(row),
					x: int64(col),
				}

			} else if terrain[row][col] == 26 {
				startCoord = Coordinate{
					y: int64(row),
					x: int64(col),
				}
			}
		}
	}

	// Start the A* search
	var openSet = mapset.NewSet[Coordinate]()
	openSet.Add(startCoord)

	var cameFrom map[Coordinate]Coordinate = map[Coordinate]Coordinate{}
	var gScore map[Coordinate]uint64 = make(map[Coordinate]uint64)
	gScore[startCoord] = 0

	var fScore map[Coordinate]uint64 = make(map[Coordinate]uint64)
	fScore[startCoord] = uint64(startCoord.Distance(endCoord))

	for true {

		if openSet.Cardinality() == 0 {
			fmt.Printf("Exhausted openSet.\n")
			break
		}

		// Find coordinate in fScore with lowest value
		var minFScore uint64 = 1000000000
		var current Coordinate
		for _, coord := range openSet.ToSlice() {
			score := fScore[coord]
			if score < uint64(minFScore) {
				current = coord
				minFScore = score

			}
		}

		openSet.Remove(current)
		// fmt.Printf("Investigating %d\n", current)

		if current == endCoord {

			path := reconstructPath(cameFrom, current)
			fmt.Printf("\tComplete path found in %d steps.\n", len(path))
			fmt.Println(path)
			PrintPath(terrain, gScore, path)
			break
		}

		for _, neighborOffset := range [4]Coordinate{
			{x: 1, y: 0},
			{x: -1, y: 0},
			{x: 0, y: 1},
			{x: 0, y: -1},
		} {
			neighborCoordinate := current.Add(neighborOffset)
			// Perform boundary checks!
			if neighborCoordinate.x < 0 || neighborCoordinate.x >= int64(len(terrain[0])) {
				// fmt.Printf("\tCoordinate %d out of bounds.\n", neighborCoordinate)
				continue
			}
			if neighborCoordinate.y < 0 || neighborCoordinate.y >= int64(len(terrain)) {
				// fmt.Printf("\tCoordinate %d out of bounds.\n", neighborCoordinate)
				continue
			}

			// fmt.Printf("\tNeighbor %d\n", neighborCoordinate)

			// Check if the neighbor is too high. If so, continue
			// fmt.Printf("\t\tComparing heights: %d, %d\n", terrain.GetHeight(neighborCoordinate), terrain.GetHeight(current))
			if int64(terrain.GetHeight(neighborCoordinate))-int64(terrain.GetHeight(current)) < -1 {
				// fmt.Printf("\t\tSkipping %d\n", neighborCoordinate)
				continue
			}

			tentative_gScore := gScore[current] + 1

			neighborGScore, inGScore := gScore[neighborCoordinate]

			if !inGScore {
				neighborGScore = 99999999
			}

			if tentative_gScore < neighborGScore {
				// This path to neighbor is better than any previous one. Record it!
				cameFrom[neighborCoordinate] = current
				gScore[neighborCoordinate] = tentative_gScore
				fScore[neighborCoordinate] = tentative_gScore + uint64(neighborCoordinate.Distance(endCoord))
				if !openSet.Contains(neighborCoordinate) {
					// fmt.Printf("\t\tAdding coordinate %d\n", neighborCoordinate)
					openSet.Add(neighborCoordinate)
				}
			} else {
				// fmt.Printf("\t\tAlready investigated %d, and this is a longer path.\n", neighborCoordinate)
			}

		}

	}
	PrintSearch(terrain, gScore)

}
