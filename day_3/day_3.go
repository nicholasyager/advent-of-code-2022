package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"

	mapset "github.com/deckarep/golang-set/v2"
)

func makeCharSet(text string) mapset.Set[string] {
	set := mapset.NewSet[string]()
	for _, element := range strings.Split(text, "") {
		set.Add(element)
	}
	return set
}

func main() {

	// Read the file into an Array of strings.
	var lines []string = make([]string, 0)
	f, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	defer f.Close()

	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		text := scanner.Text()
		lines = append(lines, text)
	}
	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}

	var letterArray string = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	// Part One
	var totalSum uint64 = 0
	for _, text := range lines {
		fmt.Printf("%s == ", text)
		compartmentSize := len(text) / 2

		compartment1 := makeCharSet(text[0:compartmentSize])
		compartment2 := makeCharSet(text[compartmentSize:])

		intersection := compartment1.Intersect(compartment2)
		item := intersection.ToSlice()[0]
		value := strings.Index(letterArray, item) + 1
		totalSum += uint64(value)
		fmt.Printf(" %s => %d\n", intersection, value)

	}

	fmt.Printf("# Total Sum = %d\n\n", totalSum)

	// Part two
	totalSum = 0

	for index := 0; index < len(lines); index += 3 {

		elf1 := makeCharSet(lines[index])
		elf2 := makeCharSet(lines[index+1])
		elf3 := makeCharSet(lines[index+2])

		intersection := elf1.Intersect(elf2).Intersect(elf3)
		item := intersection.ToSlice()[0]
		value := strings.Index(letterArray, item) + 1
		totalSum += uint64(value)
		fmt.Printf("%s | %s | %s => %s => %d\n", lines[index], lines[index+1], lines[index+2], intersection, value)

	}

	fmt.Printf("# Total Sum = %d\n", totalSum)

}
